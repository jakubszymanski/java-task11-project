package pl.sda;

import java.util.Scanner;

public class Rozw {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);


        String line = scanner.nextLine();
        int threeChars = line.length() - 3;
        if (line.length() <= 3) {
            System.out.println(line.toUpperCase());
        } else {
            String result = line.substring(0, threeChars) + line.substring(threeChars).toUpperCase();
            System.out.println(result);
        }
    }
}